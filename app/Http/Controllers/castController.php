<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class castController extends Controller
{
    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|max:255',
            'bio'  => 'required',
        ]);
        // DB::table('cast')->insert(
        //     [
        //         'nama' => $request['nama'], 
        //         'umur' => $request['umur'],
        //         'bio'  => $request['bio']
        //     ]
        // );
        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();

        return redirect('/cast');
    }

    public function index() {
        // $cast = DB::table('cast')->get();
        $cast = Cast::all();

        return view('cast.index', compact('cast'));
    }

    public function show($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);

        return view('cast.show', compact('cast'));
    }

    public function edit($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);

        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required|max:255',
            'bio'  => 'required',
        ]);

        // DB::table('cast')
        //       ->where('id', $id)
        //       ->update(
        //             [
        //               'nama' => $request['nama'],
        //               'umur' => $request['umur'],
        //               'bio'  => $request['bio']
        //             ]
        //         );

        $update = Cast::where('id', $id)->update([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function destroy($id) {
        // DB::table('cast')->where('id', '=', $id)->delete();

        Cast::destroy($id);
        return redirect('/cast');
    }
}
